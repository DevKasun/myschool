<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>DevKasun</title>
	<link rel="stylesheet" type="text/css" href="styles/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="styles/main.css">
</head>
<body>

	<div class="wrapper">
		<header>
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				<a class="navbar-brand" href="#">Navbar</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse text-right" style="flex-direction: row-reverse;" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link" href="index.php">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="gallery.php">Gallery</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="about.php">About</a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" href="contact.php">Contact</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<div class="cover">
			
		</div>
		<div class="container text-center">
			<h1>Contact</h1>
		</div>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-1">
						<img src="img/bootstrap-solid.svg" alt="scl_logo">
					</div>
					<div class="col-md-11" style="display: grid; align-items: center;">
						<p>myschool</p>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<script src="scripts/jquery-3.0.0.min.js"></script>
	<script type="text/javascript" src="scripts/bootstrap.bundle.min.js"></script>
</body>
</html>

