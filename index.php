<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>DevKasun</title>
	<link rel="stylesheet" type="text/css" href="styles/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="styles/main.css">
</head>
<body>

	<div class="wrapper">
		<header>
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				<a class="navbar-brand" href="#">Navbar</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse text-right" style="flex-direction: row-reverse;" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item active">
							<a class="nav-link" href="index.php">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="gallery.php">Gallery</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="about.php">About</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="contact.php">Contact</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<div class="cover">
			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
				    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				  </ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="img/cover_1.jpg" alt="First slide">
						<div class="carousel-caption d-none d-md-block">
							<h3>Slider 1</h3>
							<p>put text over here</p>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="img/cover_2.jpg" alt="Second slide">
						<div class="carousel-caption d-none d-md-block">
							<h3>Slider 2</h3>
							<p>put text over here</p>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="img/cover_3.jpg" alt="Third slide">
						<div class="carousel-caption d-none d-md-block">
							<h3>Slider 3</h3>
							<p>put text over here</p>
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<div class="container text-center">
			<div class="page-header">
				<h2>WELCOME</h2>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vitae suscipit quo molestiae qui vero praesentium, officia recusandae! Dolore animi quae, delectus, porro in expedita nostrum. Repudiandae id quas optio voluptatibus facere, rem quidem provident earum praesentium aperiam ea ipsam deleniti unde ducimus cupiditate iure, cum animi. Autem, molestiae, qui.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, ratione rerum accusantium qui consectetur maxime id hic minima cumque optio quo, iste accusamus incidunt error ab sit dolorem, officiis, at. Vel, quod tempore, voluptate natus officia ipsum. Dignissimos quo quas id voluptatum amet ad, molestiae placeat itaque, distinctio pariatur architecto.</p>
			<div class="row">
				<div class="col-sm-4">
					<div class="card">
						<img class="card-img-top" src="img/teacher.jpg" alt="Card image cap">
						<div class="card-body">
							<h5 class="card-title">Card title</h5>
							<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card">
						<img class="card-img-top" src="img/teacher.jpg" alt="Card image cap">
						<div class="card-body">
							<h5 class="card-title">Card title</h5>
							<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card">
						<img class="card-img-top" src="img/teacher.jpg" alt="Card image cap">
						<div class="card-body">
							<h5 class="card-title">Card title</h5>
							<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<h4>Things about school</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat aperiam enim cum nobis quam, dolorum eum inventore, sed sunt iusto quasi aliquid tempora sapiente necessitatibus vel officia delectus at doloremque! Error, consequatur vitae natus aliquid? Assumenda quam deleniti ad alias!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat aperiam enim cum nobis quam, dolorum eum inventore, sed sunt iusto quasi aliquid tempora sapiente necessitatibus vel officia delectus at doloremque! Error, consequatur vitae natus aliquid? Assumenda quam deleniti ad alias!</p>
				</div>
				<div class="col-md-7">
					<img src="img/learning.png" alt="">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="page-header">
				<h4>Lorem ipsum dolor sit amet, consectetur adipisicing.</h4>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto repudiandae nam dolore ratione obcaecati libero debitis laudantium, possimus officia nihil deleniti itaque placeat minus modi, consequatur aspernatur nulla necessitatibus natus! Impedit possimus amet tempora asperiores earum vel modi consectetur, culpa.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates reiciendis quidem nam soluta, aliquam nesciunt. Ad dolor, suscipit neque eum.</p>		
		</div>	
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-1">
						<img src="img/bootstrap-solid.svg" alt="scl_logo">
					</div>
					<div class="col-md-11" style="display: grid; align-items: center;">
						<p>myschool</p>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<script src="scripts/jquery-3.0.0.min.js"></script>
	<script type="text/javascript" src="scripts/bootstrap.bundle.min.js"></script>
</body>
</html>

